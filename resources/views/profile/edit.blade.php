@extends('admin-lte.layouts.app')


@section('content')
    <div class="col-md-8 ml-5">
            {{-- {{dd( Auth::user()->profile->photo)}} --}}
        <form 
         action="{{route('profiles.update', ['id' => Auth::user()->id])}}" 
         method="post"
         enctype="multipart/form-data"
         class="shadow-sm p-3 bg-white mx-auto">

         {{csrf_field()}}
         {{method_field('PUT')}}
         
         <div class="form-group">
            <label for="salary">Salary:</label>
            <select class="form-control" id="salary" name="salary">
                <option value="1000000-2000000" @if($profile->salary ==  '1000000-2000000') {{'selected'}} @endif>Rp.1000000-Rp.2000000</option>
                <option value="2000000-3000000" @if($profile->salary ==  '2000000-3000000') {{'selected'}} @endif>Rp.2000000-Rp.3000000</option>
                <option value="3000000-4000000" @if($profile->salary == '3000000-4000000')  {{'selected'}} @endif>Rp.3000000-Rp.4000000</option>
                <option value="4000000-5000000" @if($profile->salary == '4000000-5000000')  {{'selected'}} @endif>Rp.4000000-Rp.5000000</option>
              </select>
          </div>

          <div class="form-group">
            <label for="activity_status">Activity:</label>
            <input type="text" class="form-control" id="activity_status" name="activity" value="{{$profile->activity_status}}">
          </div>
          
          @foreach ($skills as $skill)
          <div class="form-group form-check">
              <label class="form-check-label">  
                  <input class="form-check-input" type="checkbox" name="skill[]" 
                         value="{{$skill->id}}"
                         
                         @if(!is_null($profile->skills) )
                            {{in_array($skill->id, json_decode($profile->skills)) ? ' checked' : ''}}
                         @endif
                         > 
                         {{$skill->language_program}}
            </label>
          </div>
          @endforeach


          <div class="form-group">
            <label for="prject">Project Status:</label>
            <select class="form-control" id="project" name="project">
                <option value="available" {{$profile->project_status == 'available' ? 'selected': ''}}>Available</option>
                <option value="noavailable" {{$profile->project_status == 'noavailable' ? 'selected': ''}}>No Available</option>
              </select>
          </div>

          <div class="form-group">
              <label for="photo">Photo:</label>
              <input type="file" name="photo" id="photo" class="form-control">
          </div>

          <div class="form-group">
              <label for="phone">Phone:</label>
              <input type="number" name="phone" id="phone" class="form-control" 
                    @if(!empty($profile->phone) )
                     value="{{$profile->phone}}"
                     @endif>
          </div>

          <div class="form-group">
              <label for="address">Address:</label>
              <textarea name="address" id="" cols="30" rows="10" class="form-control">{{!empty($profile->address) ? $profile->address: ''}}
              </textarea>
          </div>

          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection