@extends('admin-lte.layouts.app')


@section('content')
        <form action="{{route('admin.profileStore', [$user->id])}}" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="container-fluid">
                {{csrf_field()}}
                {{method_field('PUT')}}

                    <div class="form-group">
                        <label for="photo">Photo:</label>
                        <input type="file" name="photo" id="photo" class="form-control">
                    </div>

                        <input type="submit" value="SAVE" class="btn btn-primary ">
                    
                </div>
            </div>
        </form>
@endsection