@extends('admin-lte.layouts.app')


@section('content')
 {{-- {{dd($vacancies)}} --}}
    @foreach ($vacancies as $vacancy)
    
    <div class="container ">
        {{-- <div class="row"> --}}
            <div class="card" >
                <div class="card-header">
                    <h3><a href="{{route('vacancies.detail', [$vacancy->id])}}">{{$vacancy->title}}</a></h3>
                    <span class="bg-warning">{{$vacancy->type}}</span>
                    <span class="bg-primary">{{$vacancy->type}}</span>
                    <span class="bg-secondary">{{$vacancy->location}}</span>
                    <span class="bg-dark">{{$vacancy->salary}}</span>
                </div>
                <div class="card-body">{{str_limit($vacancy->description, 10)}}</div>
                    
            </div>
        {{-- </div> --}}
    </div>
    
    @endforeach
@endsection