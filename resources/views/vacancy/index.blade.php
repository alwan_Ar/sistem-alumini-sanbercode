@extends('admin-lte.layouts.app')


@section('content')
<div class="col-md-12">
        @if(Session::get('success'))
          <div class="alert alert-success">
            {{Session::get('success')}}
          </div>
        @endif
      </div>
       
        <div class="container bg-white p-3">
            <div class="row">
                <a href="{{route('vacancy.create')}}" class="btn btn-primary">Create Vacancy</a>
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Duration</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      @foreach ($vacancies as $vacancy)
                      <tr>
                          <td>{{$vacancy->title}}</td>
                          <td>{{$vacancy->description}}</td>
                          <td>{{$vacancy->type}}</td>
                          <td>{{$vacancy->duration}}</td>
                          <td>
                            <a href="{{route('vacancy.edit', ['id' => $vacancy->id])}}" class="btn btn-sm btn-warning">
                              <i class="fa fa-pen" title="edit"></i>
                            </a>
    
                           <form action="{{route('vacancy.destroy', [$vacancy->id])}}" method="post" 
                                 class="d-inline" onsubmit="return confirm('Apakah Anda Yakin Ingin Menghapus?')">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
    
                            <button type="submit" class="btn btn-danger btn-sm">
                              <i class="fa fa-trash"></i>
                            </button>
    
                           </form>
                          </td>
                      </tr>
                      @endforeach
                      
                    </tbody>
                  </table>
            </div>
        </div>
@endsection