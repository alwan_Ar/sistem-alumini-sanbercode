@extends('admin-lte.layouts.app')



@section('content')
    {{-- {{dd($vacancy)}} --}}

    <div class="col-md-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{$vacancy->title}}</h1>
                </div>
                <div class="col-md-12">
                    <span class="bg-warning">{{$vacancy->type}}</span>
                    <span class="bg-primary">{{$vacancy->type}}</span>
                    <span class="bg-secondary">{{$vacancy->location}}</span>
                    <span class="bg-dark">{{$vacancy->salary}}</span>
                </div>
                
                
                <p class="col-md-12">{{$vacancy->description}}</p>


                <div class="card">
                    <div class="card-header">
                        Tambahkan Komentar
                    </div>

                    @foreach ($vacancy->comments()->get() as $comment)
                    
                    <div class="card-body">
                        <h1>{{$comment->user->name}}</h1>
                        <p>{{$comment->description}}</p>    
                        <hr>
                    </div>
                    
                    @endforeach
                    
                    <div class="card-body">
                        <form action="{{route('user.comment', [$vacancy->id])}}" method="post">
                            {{csrf_field()}}
        
                            <div class="form-group">
                                <textarea name="comment" id="comment" cols="100" rows="5" class="form-control"></textarea>
                            </div>
        
                            <input type="submit" value="Kirim" class="btn btn-sm btn-primary">
                        
                        </form>
                    </div>
                </div>
               
            </div>
        </div>
    </div>

@endsection