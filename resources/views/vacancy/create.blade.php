@extends('admin-lte.layouts.app')


@section('content')
    <div class="col-md-8 ml-5">
            
        <form 
         action="{{route('vacancy.store')}}" 
         method="post"
         
         class="shadow-sm p-3 bg-white mx-auto">

         {{csrf_field()}}
         
         <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control">
        </div>

         <div class="form-group">
            <label for="type">Type:</label>
            <select class="form-control" id="type" name="type">
                <option value="part-time" >Part-time</option>
                <option value="full-time" >Full-time</option>
            </select>
          </div>

          <div class="form-group">
            <label for="duration">Duration:</label>
            <select class="form-control" id="duration" name="duration">
                <option value="6bulan" >6 Bulan</option>
                <option value="1tahun" >1 Tahun</option>
            </select>
          </div>

         <div class="form-group">
            <label for="salary">Salary:</label>
            <select class="form-control" id="salary" name="salary">
                <option value="1000000-2000000" >Rp.1000000-Rp.2000000</option>
                <option value="2000000-3000000" >Rp.2000000-Rp.3000000</option>
                <option value="3000000-4000000" >Rp.3000000-Rp.4000000</option>
                <option value="4000000-5000000" >Rp.4000000-Rp.5000000</option>
            </select>
          </div>

          <div class="form-group">
              <label for="location">Location</label>
              <input type="text" name="location" id="location" class="form-control">
          </div>

          <div class="form-group">
            <label for="skill">Skill Requirement: </label>
            <textarea name="skill" id="skill" cols="30" rows="10" class="form-control"></textarea>
         </div>

         <div class="form-group">
            <label for="desctiption">Description: </label>
            <textarea name="description" id="desctiption" cols="30" rows="10" class="form-control"></textarea>
        </div>

       
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection