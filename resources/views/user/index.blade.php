@extends('admin-lte.layouts.app')


@section('content')
{{-- @foreach ($user as $user)
    {{dd($user->profile->photo)}}
@endforeach --}}
 <div class="col-md-12">
   @if(Session::get('success'))
    <div class="alert alert-success">
      {{Session::get('success')}}
    </div>
    @endif
 </div>
<div class="container">           
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Photo</th>
              <th>Action</th>
            </tr>
          </thead>
          @foreach ($users as $user)
          <tbody>
              <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td><img src="{{ asset("storage/".$user->profile->photo) }}" alt="" class="elevation-2" witd="70px" height="150"></td>
                <td>
                  <form action="{{route('users.destroy', [$user->id])}}" method="post">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}

                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda Yakin?')">
                      <i class="fa fa-trash"></i>
                    </button>
                  </form>
                </td>
              </tr>
              
            </tbody>
          @endforeach
          
        </table>
      </div>
@endsection