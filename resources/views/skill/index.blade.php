@extends('admin-lte.layouts.app')



@section('content')
  <div class="col-md-8">
    @if(Session::get('success'))
      <div class="alert alert-success">
        {{Session::get('success')}}
      </div>
    @endif
  </div>
   
    <div class="container bg-white p-3">
        <div class="row">
            <a href="{{route('skill.create')}}" class="btn btn-primary">Create Skill</a>
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach ($skills as $skill)
                  <tr>
                      <td>{{$skill->language_program}}</td>
                      <td>
                        <a href="{{route('skill.edit', ['id' => $skill->id])}}" class="btn btn-sm btn-warning">
                          <i class="fa fa-pen" title="edit"></i>
                        </a>

                       <form action="{{route('skill.destroy', [$skill->id])}}" method="post" 
                             class="d-inline" onsubmit="return confirm('Apakah Anda Yakin Ingin Menghapus?')">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}

                        <button type="submit" class="btn btn-danger btn-sm">
                          <i class="fa fa-trash"></i>
                        </button>

                       </form>
                      </td>
                  </tr>
                  @endforeach
                  
                </tbody>
              </table>
        </div>
    </div>
@endsection