@extends('admin-lte.layouts.app')



@section('content')
    <div class="col-md-8 ml-5">
        <form action="{{route('skill.store')}}" method="post" class="shadow-sm p-3 bg-white">
            {{csrf_field()}}

            <div class="form-group">
                <label for="language">Language Program:</label>
                <input type="text" class="form-control" id="text" name="language">
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
@endsection