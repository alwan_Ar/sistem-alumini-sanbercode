<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();



Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function() {
    Route::get('/test', function () {
        return view('test');
    });

    Route::resource('skill', 'SkillController');
    Route::resource('vacancy', 'VacancyController');
    Route::resource('users', 'UserController');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/profile/{id}', 'ProfileController@profileAdmin')->name('admin.profileCreate');
    Route::put('/profile/{id}', 'ProfileController@profileAdminStore')->name('admin.profileStore');
});

Route::group(['middleware' => ['auth', 'user'], 'prefix' => 'user'], function() {
    Route::resource('/profiles', 'ProfileController');
    Route::get('/home', 'VacancyController@userHome')->name('vacancies.home');
    Route::get('/home/{id}', 'VacancyController@userVacancies')->name('vacancies.detail');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::post('/comment/{id}', 'CommentController@store')->name('user.comment');
});



Route::get('/home', 'HomeController@index')->name('home');
