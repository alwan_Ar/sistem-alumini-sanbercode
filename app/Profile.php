<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $fillable = ['user_id', 'salary', 'activity', 'project_status', 
                         'address', 'phone', 'photo'];
   
}
