<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    public $fillable = ['title', 'description', 'salary', 'type', 'duration', 
                        'location', 'skill_req', 'created_at', 'updated_at'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
