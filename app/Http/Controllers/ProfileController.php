<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skill;
use Auth;
use App\Profile;
use App\User;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Auth::user()->id;
        $profile = Profile::find($id);
        $userRelation = User::find($id);
        // dd($userRelation); 
        $skills = Skill::all();
        return view('profile.edit', compact('skills', 'profile', 'userRelation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        
        $id = Auth::user()->id;
        $profile = Profile::find($id);
        $profile->salary = $request->salary;
        $profile->activity_status = $request->activity;
        $profile->project_status = $request->project;
        $profile->phone = $request->phone;
        $profile->address = $request->address;
        
        if($request->file('photo')) {
            $photo = $request->file('photo')->store('avatars');
            $profile->photo = $photo;
        }
       
        $profile->skills = json_encode($request->skill);
        $profile->save();

        return redirect()->route('profiles.index')->with('success', 'Profile Telah Ditambahkan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profileAdmin($id){
        
        $user = User::find($id);
        return view('profile.adminprofile', compact('user'));
    }

    public function profileAdminStore(Request $request, $id) {
        // dd($request);    
        
        if($request->file('photo')){
            // dd($request); 
            $photo = $request->photo->store('avatars');
            $profile = Profile::find($id);
            $profile->photo = $photo;
            $profile->save();
            return redirect()->route('users.index');
        }

        
    }
}
