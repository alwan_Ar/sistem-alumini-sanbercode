<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vacancy;

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::all();
        return view('vacancy.index', compact('vacancies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vacancy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $vacancy = Vacancy::create([
            'title' => $request->title,
            'type' => $request->type,
            'duration' => $request->duration,
            'salary' => $request->salary,
            'location' => $request->location,
            'skill_req' => $request->skill,
            'description' => $request->description,
        ]);

        return redirect()->route('vacancy.index')->with('status', 'Vacancy Sukses Ditambahkan!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vacancy = Vacancy::find($id);
        return view('vacancy.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $vacancy = Vacancy::find($id)->update([
            'title' => $request->title,
            'type' => $request->type,
            'duration' => $request->duration,
            'salary' => $request->salary,
            'location' => $request->location,
            'skill_req' => $request->skill,
            'description' => $request->description,
        ]);

        return redirect()->route('vacancy.index')->with('success', 'Vacancy Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vacancy::destroy($id);

        return redirect()->back()->with('success', 'Data Berhasil Dihapus!!');
    }


    public function userHome(){
        $vacancies = Vacancy::all();
        return view('vacancy.userhome', compact('vacancies'));
    }

    public function userVacancies($id){
        $vacancy = Vacancy::find($id);
        return view('vacancy.userDetail', compact('vacancy'));
    }
}
