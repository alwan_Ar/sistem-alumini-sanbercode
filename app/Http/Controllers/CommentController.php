<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;
class CommentController extends Controller
{
    public function store(Request $request, $id){
        $comment = Comment::create([
            'vacancy_id' => $id,
            'user_id' => Auth::user()->id,
            'description' => $request->comment,
        ]);

        return redirect()->back();
    }
}
