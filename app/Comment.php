<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $fillable = ['vacancy_id', 'user_id', 'description'];
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function vacancy(){
        return $this->belongsTo('App\Vacancy');
    }
}
