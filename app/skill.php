<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class skill extends Model
{
    public $table = 'skills';
    public $fillable = ['language_program', 'created_at', 'updated_at'];
}
